import processing.core.PVector;
import simulation.Particle;
import simulation.Structure;
import simulation.constraints.Pin;
import simulation.constraints.Stick;


public class Tentacle extends Structure 
{
	public Pin firstpin;
	public Tentacle()
	{
		Particle firstParticle = new Particle(new PVector(200, 0));
		firstpin = new Pin(firstParticle);
		addConstraint(firstpin);
		addParticle(firstParticle);
		for(int i = 0; i < 10; i++)
		{
			Particle p = new Particle(new PVector(200, i*20));
			
			addParticle(p);
		}
		
		for(int i = 1; i < getParticles().size(); i++)
		{
			addConstraint(new Stick(getParticles().get(i-1), getParticles().get(i), 10));
		}
	}
}
