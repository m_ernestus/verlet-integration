import processing.core.PVector;
import simulation.Particle;
import simulation.Structure;
import simulation.constraints.Pin;
import simulation.constraints.RotatingPin;
import simulation.constraints.Stick;


/**
 * Theo Jansens leg structure in my physics simulation!
 * @author Maximilian Ernestus
 *
 */
public class TheoLeg extends Structure
{
	final float a = 38, b = 41.5f, c = 39.3f, d = 40.1f, e = 55.8f, f = 39.4f, g = 36.7f, h = 65.7f, i = 49, j = 50, k = 61.9f, l=7.8f, m=15;
	
	public Particle foot;
	public Particle p1, p2, p3, p4, p5;
	public TheoLeg(PVector pos)
	{
		Particle center = new Particle(new PVector(pos.x, pos.y));
		addParticle(center);
		addConstraint(new Pin(center));
		
		Particle rotating = new Particle(PVector.add(pos, new PVector(a, -l)));
		addParticle(rotating);
		addConstraint(new RotatingPin(rotating, m));
		
		p1 = new Particle(PVector.add(pos, new PVector(0, -b)));
		p2 = new Particle(PVector.add(pos, new PVector(-d, 0)));
		p3 = new Particle(PVector.add(pos, new PVector(-g, c)));
		p4 = new Particle(PVector.add(pos, new PVector(0, c)));
		p5 = new Particle(PVector.add(pos, new PVector(0, c+i)));
		
		
		
		addParticle(p1);
		addParticle(p2);
		addParticle(p3);
		addParticle(p4);
		addParticle(p5);
		
		addConstraint(new Stick(p1, rotating, j));
		addConstraint(new Stick(p1, center, b));
		addConstraint(new Stick(p1, p2, e));
		
		addConstraint(new Stick(rotating, p4, k));
		
		addConstraint(new Stick(p2, center, d));
		addConstraint(new Stick(p2, p3, f));
		
		addConstraint(new Stick(center, p4, c));
		
		addConstraint(new Stick(p3, p4, g));
		addConstraint(new Stick(p3, p5, h));
		
		addConstraint(new Stick(p4, p5, i));
		
		foot = p5;
	}
}
