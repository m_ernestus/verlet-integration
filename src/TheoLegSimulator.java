import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;


public class TheoLegSimulator extends Simulator 
{
	TheoLeg leg;
	
	public void setup()
	{
		super.setup();
		leg = new TheoLeg(new PVector(width/2, height/2));
		sim.addStructure(leg);
	}
	
	public void draw()
	{
		super.draw();
//		for(Particle p : leg.getParticles()) trackParticle(p, 180);
	}
	
	public static void main(String args[])
	{
		PApplet.main(new String[] { "TheoLegSimulator" });
	}
}
