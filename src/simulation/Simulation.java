package simulation;

import java.util.LinkedList;

import simulation.constraints.Constraint;


/**
 * This class represents a physics simulation with all its constraints and particles.
 * It also keeps track of the time and tries its best to make sure that the constraints 
 * are satisfied.
 * 
 * @author Maximilian Ernesuts
 *
 */
public class Simulation 
{
	LinkedList<Particle> particles = new LinkedList<Particle>();
	LinkedList<Constraint> constraints = new LinkedList<Constraint>();
	private long time = 0;
	
	public float nextTimeStep()
	{
		verletUpdates();
		float err = satisfyConstraints();
		for(Constraint c : constraints)
			c.endOfTimestep();
		time++;
		return err;
	}
	
	public void verletUpdates()
	{
		for(Particle p : particles)
			p.verletUpdate();
	}
	
	/**
	 * Tries to satisfy all constraints in the system until the error in the system
	 * goes below a certain threshold or we exceed a certain number of iterations.
	 */
	public float satisfyConstraints()
	{
		float err = Float.MAX_VALUE;
		int iterations = 0;
		while(err > 0.01 && iterations < 1000)
		{
			err = 0;
			for(Constraint c : constraints)
				err += c.satisfyConstraint();
			iterations++;
		}
		return err;
	}
	
	public void addConstraint(Constraint c)
	{
		constraints.add(c);
	}
	
	public void addParticle(Particle p)
	{
		particles.add(p);
	}
	
	public void addStructure(Structure s)
	{
		constraints.addAll(s.getConstraints());
		particles.addAll(s.getParticles());
	}

	public LinkedList<Particle> getParticles() {
		return particles;
	}

	public LinkedList<Constraint> getConstraints() {
		return constraints;
	}
	
	/**
	 * Removes all particles, all constraints and resets the time.
	 */
	public void clear()
	{
		particles.clear();
		constraints.clear();
		time = 0;
	}

	public long getTime() {
		return time;
	}
}
