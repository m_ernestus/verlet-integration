package simulation;

import processing.core.PVector;


/**
 * This class represents a particle in our physics simulation.
 * It uses verlet integration.
 * @author Maximilian Ernestus
 *
 */
public class Particle 
{
	public PVector pos;
	public PVector oldPos;
	public PVector externalForces;
	public float friction = 0.1f;
	
	public Particle(PVector pos)
	{
		this.pos = pos;
		this.oldPos = pos.get();
		this.externalForces = new PVector(0, -9f);
	}
	
	public void verletUpdate() //TODO: make private later
	{
		PVector tmp = pos.get(); //makes a copy of poss
		
		//x' = 2 * x - oldX + a = x + (x-oldX) + a
//		pos.mult(2f);
//		pos.sub(oldPos);
//		pos.add(externalForces);
		
		PVector newTerm = PVector.sub(pos, oldPos);
		newTerm.add(externalForces);
		newTerm.mult(friction);
		
		pos.add(newTerm);
		
		
		
		oldPos = tmp;
	}
	
	@Override
	public String toString()	
	{
		return "Particle: " + pos;
	}
}
