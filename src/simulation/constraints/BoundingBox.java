package simulation.constraints;

import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;


/**
 * This constraint keeps its particle inside a predefined bounding box.
 * E.g. it keeps your particles on the screen!
 * @author Maximilian Ernestus
 *
 */
public class BoundingBox extends Constraint {
	
	public float x, y, width, height;
	
	public BoundingBox(Particle p, float x, float y, float width, float height)
	{
		super(p);
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public BoundingBox(Particle p, float width, float height)
	{
		this(p, 0, 0, width, height);
	}
	
	public BoundingBox(Particle p, PApplet a)
	{
		this(p, a.getWidth()-1, a.getHeight()-1);
	}
	
	@Override
	public float satisfyConstraint() 
	{
		PVector before = particle.pos.get();
		particle.pos.x = PApplet.max(particle.pos.x, x);
		particle.pos.x = PApplet.min(particle.pos.x, x+width);
		particle.pos.y = PApplet.min(particle.pos.y, y+height);
		particle.pos.y = PApplet.max(particle.pos.y, y);
		return PApplet.abs(before.x - particle.pos.x) + PApplet.abs(before.y - particle.pos.y);
	}
}