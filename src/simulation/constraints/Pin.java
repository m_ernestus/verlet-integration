package simulation.constraints;
import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;


/**
 * This constraint pins its particle to its current location.
 * But if something else pulls on the particle too hard it will break!
 * TODO: make the breaking point more flexible!
 * @author Maximilian Ernestus
 *
 */
public class Pin extends Constraint {

	public PVector pos;
	public boolean broken = false;
	private float lastErr = 0;
	
	public Pin(Particle p)
	{
		super(p);
		pos = p.pos.get();//we copy the current pin location here
	}
	
	@Override
	public float satisfyConstraint() 
	{
		if(!broken)
		{
			lastErr = PApplet.abs(particle.pos.x - pos.x) + PApplet.abs(particle.pos.y - pos.y);
			
			particle.pos = pos.get();
			return lastErr;
		}
		else
			return 0;
	}
	
	@Override
	public void endOfTimestep()
	{
		if(lastErr > 10) //rotating pins should not break 
			broken = true;
	}
	
	/**
	 * Redefines the position where the particle should be pinned to.
	 * @param pos
	 */
	public void setPos(PVector pos)
	{
		this.pos = pos;
	}

}
