package simulation.constraints;
import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;


/**
 * This is a special pin, that rotates around a certain point with a certain radius.
 * It can not break.
 * It brings some movement into your structure!
 * TODO: Make the speed of rotation flexible.
 * @author Maximilian Ernestus
 *
 */
public class RotatingPin extends Pin {

	private float radius, angle;
	private float stepsPerRevolution = -180;
	private PVector rotationCenter;
	public RotatingPin(Particle p, float radius) {
		super(p);
		this.radius = radius;
		this.rotationCenter = p.pos.get();
	}

	@Override
	public float satisfyConstraint() 
	{
		setPos(new PVector(PApplet.cos(angle)*radius + rotationCenter.x, PApplet.sin(angle)*radius + rotationCenter.y));
		//angle = (PApplet.TWO_PI/(float)stepsPerRevolution)*(time%stepsPerRevolution);
		return super.satisfyConstraint();
	}
	
	@Override
	public void endOfTimestep()
	{
		angle += (PApplet.TWO_PI/(float)stepsPerRevolution);
	}

	public PVector getRotationCenter() {
		return rotationCenter;
	}

	public float getRadius() {
		return radius;
	}
}
