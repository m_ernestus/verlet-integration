package simulation.constraints;

import simulation.Particle;



/**
 * This class defines the propertys of a general purpose constraint in our world.
 * 
 * @author Maximilian Ernestus
 */
public abstract class Constraint 
{
	/**
	 * A list of particles where the contraint acts on.
	 */
	public Particle[] particles;
	
	/**
	 * Just a link to the first element in the list of particles for convenience.
	 */
	public Particle particle;
	
	public Constraint(Particle... particles)
	{
		this.particles = particles;
		this.particle = particles[0];
	}
	
	/**
	 * Changes the properties of the particles under its influence to meet the criteria of the constraint.
	 * @return The amount of adjustment that has been done.
	 */
	abstract public float satisfyConstraint();
	
	/**
	 * To be called after all the satisfyConstraint calls at the end of the timestep.
	 */
	public void endOfTimestep()
	{
		
	}
}
