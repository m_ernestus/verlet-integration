package simulation.constraints;
import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;



/**
 * This constraint acts like a stick and keeps two particles at a fixed distance.
 * @author Maximilian Ernestus
 *
 */
public class Stick extends Constraint {

	float distance;
	
	public Stick(Particle p1, Particle p2)
	{
		super(p1, p2);
		distance = p1.pos.dist(p2.pos);
	}
	
	public Stick(Particle p1, Particle p2, float distance)
	{
		super(p1, p2);
		this.distance = distance;
	}
	
	@Override
	public float satisfyConstraint() 
	{
		//Here we move the two particles apart from each other or closer to each other
		//To satisfy the distance constraint.
		PVector p1 = particles[0].pos;
		PVector p2 = particles[1].pos;
		PVector delta = PVector.sub(p1, p2);
		float deltaLength = delta.mag();
		float diff = (deltaLength - distance) / deltaLength;
		delta.mult(diff/2);
		float err = 2*(PApplet.abs(delta.x) + PApplet.abs(delta.y));
		p1.sub(delta);
		p2.add(delta);
		return err;
	}
}
