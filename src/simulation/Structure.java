package simulation;
import java.util.LinkedList;

import simulation.constraints.Constraint;



/**
 * This class represents just a container for particles and constraints.
 * 
 * @author Maximilian Ernestus
 *
 */
public abstract class Structure 
{
	LinkedList<Particle> particles = new LinkedList<Particle>();
	LinkedList<Constraint> constraints = new LinkedList<Constraint>();
	
	public void addConstraint(Constraint c)
	{
		constraints.add(c);
	}
	
	public void addParticle(Particle p)
	{
		particles.add(p);
	}
	
	public LinkedList<Particle> getParticles()	
	{
		return particles;
	}
	
	public LinkedList<Constraint> getConstraints()	
	{
		return constraints;
	}
	
	public void clear()
	{
		particles.clear();
		constraints.clear();
	}
}
