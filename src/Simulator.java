import java.util.HashMap;
import java.util.LinkedList;

import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;
import simulation.Simulation;
import simulation.constraints.BoundingBox;
import simulation.constraints.Constraint;
import simulation.constraints.Pin;
import simulation.constraints.RotatingPin;
import simulation.constraints.Stick;

/**
 * Renders most of the world elements and keeps the time going. You should
 * overwrite setup() but call super.setup(). You also will need to add a weird
 * main() like below.
 * 
 * @author Maximilian Ernestus
 */
public class Simulator extends PApplet {
	Simulation sim = new Simulation();

	public void setup() {
		size(600, 600);
		background(0);
//		hint(ENABLE_ACCURATE_2D);
		hint(ENABLE_STROKE_PERSPECTIVE);
	}

	public void draw() {
		fill(0);
		noStroke();
		rect(0, 0, width, height);

		// long startTime = millis();
		sim.nextTimeStep();
		// System.out.printf("%d ms\n", millis() - startTime);

		for (Particle p : sim.getParticles()) {
			drawParticle(p);
		}
		for (Constraint c : sim.getConstraints())
			drawConstraint(c);
	}

	HashMap<Particle, LinkedList<PVector>> traces = new HashMap<Particle, LinkedList<PVector>>();

	/**
	 * If called on every draw it will make the particle leave a trace of a
	 * certain length.
	 * 
	 * @param p
	 * @param howLong
	 */
	public void trackParticle(Particle p, int howLong) {
		// We store the trace in a LinkedList
		if (traces.get(p) == null) {
			traces.put(p, new LinkedList<PVector>());
		}
		LinkedList<PVector> trace = traces.get(p);

		// We add the current positin to the trace
		trace.add(p.pos.get());

		// We remove old items from our trace to get the desired length
		while (trace.size() > howLong)
			trace.removeFirst();

		drawTrace(trace);
	}

	public void drawTrace(LinkedList<PVector> trace, int color) {
		// We draw the trace on the screen
		ellipseMode(CENTER);
		noStroke();
		fill(color);
		for (PVector v : trace) 
			ellipse(v.x, v.y, 2, 2);
	}
	
	public void drawTrace(LinkedList<PVector> trace)
	{
		drawTrace(trace, color(0, 130, 100));
	}

	public void drawParticle(Particle p) {
		fill(255, 50);
		noStroke();
		ellipseMode(CENTER);
		ellipse(p.pos.x, p.pos.y, 5, 5);
	}

	public void drawConstraint(Constraint c) {
		if (c instanceof BoundingBox) {
			BoundingBox b = (BoundingBox) c;
			rectMode(CORNER);
			stroke(color(255, 0, 0));
			noFill();
			rect(b.x, b.y, b.width, b.height);
		}

		if (c instanceof Stick) {
			Stick s = (Stick) c;
			stroke(255);
			PVector p1 = s.particles[0].pos;
			PVector p2 = s.particles[1].pos;
			line(p1.x, p1.y, p2.x, p2.y);
		}

		if (c instanceof Pin) {
			Pin p = (Pin) c;
			fill(255, 0, 0);
			if (p.broken) // make it yellow if it is broken
				fill(255, 255, 0);
			noStroke();
			ellipseMode(CENTER);
			ellipse(p.pos.x, p.pos.y, 3, 3);
			if (p instanceof RotatingPin) {
				RotatingPin r = (RotatingPin) p;
				PVector cro = r.getRotationCenter();
				stroke(255);
				line(cro.x - 2, cro.y - 2, cro.x + 2, cro.y + 2);
				line(cro.x + 2, cro.y - 2, cro.x - 2, cro.y + 2);
				stroke(255, 50);
				noFill();
				ellipseMode(CENTER);
				ellipse(cro.x, cro.y, r.getRadius() * 2, r.getRadius() * 2);
			}
		}
	}

	public static void main(String args[]) {
		PApplet.main(new String[] { "Simulator" });
	}
}
