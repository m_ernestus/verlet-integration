import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;
import simulation.constraints.Pin;
import simulation.constraints.Stick;


public class TentacleSimulator extends Simulator {

	Tentacle t;
	Pin p;
	public void setup()
	{
		super.setup();
		Particle[] ps = new Particle[10];
		
		for(int i = 0; i < ps.length; i++)
		{
			ps[i] = new Particle(new PVector(50, 50 + i * 40));
			
			sim.addParticle(ps[i]);
		}
		
		for(int i = 1; i < ps.length; i++)
			sim.addConstraint(new Stick(ps[i-1], ps[i]));
		
		
		
		p = new Pin(ps[0]);
		sim.addConstraint(p);
	}
	
	public void draw()
	{
		super.draw();
		p.setPos(new PVector(mouseX, mouseY));
		text("" + frameRate, 20, 20);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		PApplet.main(new String[] { "TentacleSimulator" });
	}
}
