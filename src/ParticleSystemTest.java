import processing.core.PApplet;
import processing.core.PVector;
import simulation.Particle;
import simulation.constraints.Gravitation;


public class ParticleSystemTest extends Simulator 
{
	public void setup()
	{
		super.setup();
		
	}
	
	public void draw()
	{
		super.draw();
		if(mousePressed)
		{
			Particle p = new Particle(new PVector(mouseX, mouseY));
			sim.addParticle(p);
		}
	}
	
	public static void main(String[] args) 
	{
		PApplet.main(new String[] { "ParticleSystemTest" });
	}
}
